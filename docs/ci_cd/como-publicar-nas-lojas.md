---
id: como-publicar-nas-lojas
title: Como publicar uma versão nas lojas
---

# 🚀


:::caution

**Atenção!** - Se você nunca publicou uma versão antes, peça ajuda à equipe. **NÃO TENTE** publicar uma versão de produção sem supervisão.

:::

## Play Store (Teste interno)

Para enviar um **Android App Bundle** manualmente para a **Play Store**, siga os seguintes passos:

1. Acesse o painel do app na [Play Store](https://play.google.com/apps/publish), utilizando uma conta com privilégios suficiente para lançar novas versões (solicitar à equipe)
2. No menu lateral esquerdo, acesse "Teste/Teste Interno"

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/856c08c8cdf1457e4d94f06bc9ed1694/image.png" />

1. Clique no botão azul no canto direito superior "Criar nova versão"
2. Na sessão "Pacotes de apps", clique em "Enviar" para selecionar o arquivo ".aab" que foi gerado (para saber mais sobre como gerar versões, acesse [Como gerar versões de apps na Lighthouse](./como-gerar-versoes.md))
3. Role a página e verifique se o "Nome da versão" foi preenchido corretamente, de acordo com o que você configurou nos arquivos locais do seu projeto (E.x: "11502 (2.0.1)")
4. Preencha a caixa de texto "Notas da versão", listando todas as alterações feitas à compilação. Estas serão mostradas na Play Store para os usuários que forem baixar o app

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/d073c63dd1e3ed664e44620b451e081a/image.png" />

5. Clique em "Salvar" no canto inferior direito
6. Clique em "Avaliar versão" (ao lado de "Salvar")
7. Verifique se todas as alterações estão OK e clique em "Iniciar lançamento para teste interno"

## TestFlight

// todo...
