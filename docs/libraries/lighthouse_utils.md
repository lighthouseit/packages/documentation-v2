---
id: lighthouse_utils
title: Lighthouse Utils
---

A biblioteca [Lighthouse Utils](https://gitlab.com/lighthouseit/packages/lighthouse-utils) tem como objetivo expor funções utilitárias, que podem ser utilizadas entre todos os nossos projetos. Essa lib é compatível com qualquer projeto NodeJS, sendo possível instalá-la tanto no *backend*, quanto no *frontend* (Ex. Ant Design Pro, React Native, etc).

## Instalação

A biblioteca está publicada no NPM, então para adicioná-la ao projeto, basta executar o comando:

```
npm i @lighthouseapps/utils
```

ou, caso esteja usando `yarn`, execute o comando

```
yarn add @lighthouseapps/utils
```

____

## Métodos


### validateCPF

Retorna um `boolean` se o CPF informado é válido ou não.

```javascript
import { validateCPF } from '@lighthouseapps/utils';

validateCPF('03455455787'); // true/false

```

### maskCPF

Adicionar máscara ao número de CPF informado.


```javascript
import { maskCPF } from '@lighthouseapps/utils';

maskCPF('28832326280'); // 288.323.262-80

```

### unmaskCPF

Remove a máscara do número de CPF informado, retornando apenas os números.


```javascript
import { unmaskCPF } from '@lighthouseapps/utils';

unmaskCPF('288.323.262-80'); // 28832326280

```

### validateCNPJ

Retorna um `boolean` se o CNPJ informado é válido ou não.

```javascript
import { validateCNPJ } from '@lighthouseapps/utils';

validateCNPJ('17702663000150'); // true/false

```

### maskCNPJ

Adicionar máscara ao número de CNPJ informado.


```javascript
import { maskCNPJ } from '@lighthouseapps/utils';

maskCNPJ('17702663000150'); // 17.702.663/0001-50

```

### unmaskCNPJ

Remove a máscara do número de CNPJ informado, retornando apenas os números.


```javascript
import { unmaskCNPJ } from '@lighthouseapps/utils';

unmaskCNPJ('17.702.663/0001-50'); // 17702663000150

```


### validateCCNumber

Retorna um `boolean` se o número de cartão de crédito informado é válido ou não.

```javascript
import { validateCCNumber } from '@lighthouseapps/utils';

validateCCNumber('5598928852411791'); // true/false

```

### validateCCExpiry

Retorna um `boolean` se a data de expiração do cartão de crédito informado é válido ou não.

```javascript
import { validateCCExpiry } from '@lighthouseapps/utils';

validateCCExpiry('12/22'); // true/false

```

### getCCNumberInfo

Retorna um `object` com informações sobre o número de cartão de crédito informado.

```javascript
import { getCCNumberInfo } from '@lighthouseapps/utils';

getCCNumberInfo('5485775006283622'); 

/*
retorna:

{
    card: {
      niceType: 'Mastercard',
      type: 'mastercard',
      patterns: [[51, 55], [2221, 2229], [223, 229], [23, 26], [270, 271], 2720],
      gaps: [4, 8, 12],
      lengths: [16],
      code: { name: 'CVC', size: 3 },
      matchStrength: 2,
    },
    isPotentiallyValid: false,
    isValid: false,
  };
*/

```

### initials

Retorna uma `string` com as letras iniciais do nome informado

```javascript
import { initials } from '@lighthouseapps/utils';

initials('Lighthouse apps'); // LA

```


### validatePhone

Retorna um `boolean` se o número de telefone informado é válido ou não.

```javascript
import { validatePhone } from '@lighthouseapps/utils';

validatePhone('(11) 99989-8887'); // true/false

```

### maskPhone

Adicionar máscara ao número de telefone informado.


```javascript
import { maskPhone } from '@lighthouseapps/utils';

maskPhone('11999898887'); // (11) 99989-8887

```

### unmaskPhone

Remove a máscara do número de telefone informado, retornando apenas os números.


```javascript
import { unmaskPhone } from '@lighthouseapps/utils';

unmaskPhone('(11) 99989-8887'); // 11999898887

```

### capitalize

Torna maiúscula apenas a primeira letra de cada palavra da frase informada.


```javascript
import { capitalize } from '@lighthouseapps/utils';

capitalize("LIGHTHOUSE - CRIAMOS APPS INSCRIVEIS"); // Lighthouse - Criamos Apps Incriveis

```

____

## Brazilian utils


Nossa lib `Lighthouse Utils` expoe algumas funções diretamente da biblioteca [brazilian-utils](https://brazilian-utils.com.br/#/).

São elas:


### isValidBoleto

Retorna um `boolean` se o número de boleto informado é válido ou não.

```javascript

import { isValidBoleto } from '@lighthouseapps/utils';

isValidBoleto('00190000090114971860168524522114675860000102656'); // true

```

### formatBoleto

Adiciona a máscara com pontuação ao número de boleto informado.

```javascript

import { formatBoleto } from '@lighthouseapps/utils';

formatBoleto('00190000090114971860168524522114675860000102656');

```

### isValidCEP

Retorna um `boolean` se o CEP informado é válido ou não.

```javascript
import { isValidCEP } from '@lighthouseapps/utils';

isValidCEP('92500000'); // true

```

### formatCEP

Adicionar máscara ao número de CEP informado.

```javascript
import { formatCEP } from '@lighthouseapps/utils';

formatCEP('92500000'); // 92500-000

```

### isValidCNPJ

Retorna um `boolean` se o CNPJ informado é válido ou não.

```javascript
import { isValidCNPJ } from '@lighthouseapps/utils';

isValidCNPJ('15515147234255'); // true/false

```

### formatCNPJ

Adicionar máscara ao número de CNPJ informado.

```javascript
import { formatCNPJ } from '@lighthouseapps/utils';

formatCNPJ('245222000174'); // 24.522.200/0174

```

### isValidCPF

Retorna um `boolean` se o CPF informado é válido ou não.

```javascript
import { isValidCPF } from '@lighthouseapps/utils';

isValidCPF('155151475'); // true/false

```

### formatCPF

Adicionar máscara ao número de CPF informado.

```javascript
import { formatCPF } from '@lighthouseapps/utils';

formatCPF('746506880'); // 746.506.880

```

### isValidPIS

Retorna um `boolean` se o CPF informado é válido ou não.

```javascript
import { isValidPIS } from '@lighthouseapps/utils';

isValidPIS('12056412547'); // true/false

```

### isValidEmail

Retorna um `boolean` se o CPF informado é válido ou não.

```javascript
import { isValidEmail } from '@lighthouseapps/utils';

isValidEmail('john.doe@hotmail.com'); // true


```
