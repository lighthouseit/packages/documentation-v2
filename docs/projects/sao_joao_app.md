---
id: sao_joao_app
title: São João App
---

# 💊

[![Build Status](https://app.bitrise.io/app/51812660723c8f65/status.svg?token=f953JXYh1iW8sIS-_Wu9rQ&branch=master)](https://app.bitrise.io/app/51812660723c8f65)

:::important

🚧 **Em construção** 🚧 - O app está em estágio de reescrita e essa documentação pode sofrer alterações a qualquer momento.

:::

# Desenvolvimento local - app 

**Requisitos:**

- Acessos aos ambientes da São João
- [Forticlient Free VPN](https://www.fortinet.com/support/product-downloads#vpn)
- [Git bash](https://git-scm.com/downloads)
- [yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable)
- Ambiente [React Native](https://reactnative.dev/docs/environment-setup) configurado

____

## Entendendo os ambientes

As variáveis de ambiente devem ser definidas no arquivo `.env`, na raiz do projeto. As variáveis que mudarão de acordo com o modo de execução são:

`RN_API_URL`: URL base do BFF

`RN_MAGENTO_ACCESS_TOKEN`: Token do Magento

`RN_CARDS_ACCESS_TOKEN`: Token da API responsável pelo gerenciamento dos cartões de crédito

`RN_CIELO_ENVIRONMENT`: Define o tipo de ambiente do gateway de pagamento da Cielo


O código de cada ambiente está **comentado** no arquivo. Basta descomentá-lo e executar o comando pra *limpar o cache do React Native* para começar a trabalhar em cima do ambiente desejado.


:::tip

Para limpar o cache execute o comando 

`npx react-native start --reset-cache` 

após alternar entre algum ambiente no arquivo `.env`

:::


:::caution

**Atenção**: Ao desenvolver localmente, tenha certeza de que o ambiente está sendo executado como `HML`

:::

## Como rodar o projeto

### Instale as dependências

```
yarn
```

ou

```
npm i
```

### Android

```
yarn android:run
```

ou

```
npm run android:run
```

ou

```
npx react-native run-android
```

### iOS


```
yarn ios
```

ou

```
npm run ios
```

ou

```
npx react-native run-ios
```

## Publicação de versões

O app está configurado no [Bitrise](https://app.bitrise.io/app/51812660723c8f65#/builds) (você precisará de acesso ao painel para acessar).

### Gerando um build no Bitrise

#### Builds Automáticos

Para iniciar um build de produção, para ambas as plataformas, basta dar um `push` para a branch da versão, seguindo a seguinte nomenclatura: `release/*.*.*`

**Exemplo**: release/1.2.3

Ao fazer isso, o Bitrise irá iniciar o *step* `Production`, que por sua vez, gera os builds de ambas as plataformas, iniciando os steps `Production-AAB` (Android) e `iOS`

#### Sobre os steps do Bitrise

**Production-AAB**: Gera um Android App Bundle, que ficará disponível na aba "APPS & ARTIFACTS" do build

**iOS**: Irá gerar um IPA e enviá-lo automaticamente para a AppStore connect, para que seja então avalidado e enviado para produção ou TestFlight


:::danger

**Atenção**: No iOS, o hermes está desabilitado por padrão e será habilitado automaticamente via Bitrise. 

Caso você esteja gerando uma versão para produção MANUALMENTE, **habilite o Hermes** no arquivo PodFile antes de gerar o build.

:::

