module.exports = {
  title: 'Lighthouse Docs',
  tagline: 'Documentação dos projetos internos da equipe Lighthouse',
  url: 'https://lighthouseit.gitlab.io/',
  baseUrl: '/packages/documentation-v2/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'lighthouseit', // Usually your GitHub org/user name.
  projectName: 'lighthouse-docs', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Lighthouse Docs',
      logo: {
        alt: 'Lighthouse Logo',
        src: 'img/logo_red.png',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        { to: 'blog', label: 'Blog', position: 'left' },
        // {
        //   href: 'https://github.com/facebook/docusaurus',
        //   label: 'GitHub',
        //   position: 'right',
        // },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Introdução',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Redes Sociais',
          items: [
            {
              label: 'Facebook',
              href: 'https://www.facebook.com/lighthouseitcombr',
            },
            {
              label: 'Instagram',
              href: 'https://www.instagram.com/lighthouseapps/',
            },
            {
              label: 'LinkedIn',
              to: 'https://www.linkedin.com/company/lighthouseapps/',
            },
          ],
        },
        {
          title: 'Contato',
          items: [
            {
              label: 'Site Oficial',
              href: 'https://lighthouseit.com.br/',
            },
            {
              label: 'comercial@lighthouseit.com.br',
              href: 'mailto:comercial@lighthouseit.com.br',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} LighthouseIt. Feito com Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/lighthouseit/packages/documentation-v2/-/tree/master/docs',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/lighthouseit/packages/documentation-v2/-/tree/master/blog',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
