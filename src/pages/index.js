import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'Discuta com a Equipe',
    imageUrl: 'img/first_item.svg',
    description: (
      <>
        Pergunte e discuta com a equipe sobre os tópicos da documentação, dê sugestões
        e críticas construtivas são sempre bem vindas!
      </>
    ),
  },
  {
    title: 'Foque no que Importa',
    imageUrl: 'img/second_item.svg',
    description: (
      <>
        A documentação foi feita para ajudar a focar no que mais utilizamos na empresa,
        vá em frente clicando em <code>documentação</code> e comece agora.
      </>
    ),
  },
  {
    title: 'Aumente sua Produtividade',
    imageUrl: 'img/third_item.svg',
    description: (
      <>
        Seguindo os mesmos padrões e organizando nossos códigos conseguimos aumentar nossa
        produtividade e gerar mais resultados! 🚀
      </>
    ),
  },
];

function Feature({ imageUrl, title, description }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <img
            src={useBaseUrl(`img/logo_red.png`)}
            height={100}
            className={styles.logo}
            alt="Logo"
          />
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          {/* <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Comece Agora
            </Link>
          </div> */}
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
