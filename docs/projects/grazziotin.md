---
id: grazziotin
title: Grazziotin
---

# Como publicar uma versão do sistema

**Requisitos:**

- Arquivo de acessos (solicitar ao time)
- Forticlient Free VPN
- Git bash

____

## Entendendo os ambientes

Os ambientes de produção e homologação estão hospedados em servidores internos da Grazziotin e os arquivos podem ser acessados via SSH, quando conectado à VPN.

- IP de Homologação: 192.168.200.76
- IP de Produção: 192.168... (https://app.grazziotin.com.br)

Com isso, é possível acessar os projetos diretamente pelo IP

**E.x homologação:**
- website - http://192.168.200.76:8010/site/home
- admin - http://192.168.200.76:8020/administracao/home
- API - http://192.168.200.76:8000

**NOTA**: Lembre-se de que para acessar o ambiente diretamente através do IP, é preciso estar conectado à VPN

## Deploy

### Scripts de deploy

Tanto na [API](https://gitlab.com/lighthouseit/grazziotin/grazziotin-backend), quanto em ambos os fronts ([website](https://gitlab.com/lighthouseit/grazziotin/grazziotin-react) e [admin](https://gitlab.com/lighthouseit/grazziotin/grazziotin-admin)), existem dois scripts para realizar a publicação de uma nova versão:

```
yarn deploy
```

e

```
yarn deploy:hmg
```

Essencialmente, o que esses scripts fazem é: Executam o processo de build do projeto e enviam os arquivos gerados, via FTP, para o servidor de produção ou de homologação, respectivamente.

**NOTA:** No projeto da API, o script `yarn deploy` irá alterar a propriedade `envMode` para `production` no `package.json` automaticamente antes de publicar os arquivos. Da mesma forma, o script `yarn deploy:hmg` irá alterar a prop para `homolog`.

### Realizando o deploy

O processo de deploy é tão simples quanto:

1. Conectar à VPN utilizando os dados de acesso
2. Executar o script de deploy desejado na pasta raiz do projeto
3. Colar a senha do SSH informada no arquivo de acessos
4. Reiniciar o processo remoto, caso necessário.

----

## Ciclo de vida dos serviços

O sistema é controlado em ambos os ambientes via [PM2](https://pm2.keymetrics.io/docs/usage/quick-start/). Os comandos básicos são

- `pm2 stop nome_do_projeto` - Encerra o processo
- `pm2 start nome_do_projeto` - Inicia o processo
- `pm2 restart nome_do_projeto` - Reinicia o processo

### Verificar execução dos processos

```
pm2 status
```

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/38f49f3b3064da9c48dccd8f5fd1a696/image.png" />

### Reiniciar API

```
pm2 restart services
```

### Reiniciar Site

```
pm2 restart website
```

### Reiniciar Painel Admin

```
pm2 restart admin
```

# Apontando a API para prod ou homolog localmente

A propriedade `envMode` do arquivo `package.json` é responsável por dizer ao projeto se ele deve referenciar o banco de dados de produção ou homologação, além de apontar também para o ambiente de produção ou sandbox da Braspag.

Os 3 possíveis valores de `envMode` são:

- production
- homolog
- development

No momento, `homolog` e `development` produzem o mesmo comportamento, sendo `development` o valor padrão.
