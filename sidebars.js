module.exports = {
  someSidebar: {
    'Documentação': [
      "documentation/doc1",
      "documentation/doc2",
    ],
    Design: ["design/doc1"],
    Bibliotecas: [
      "libraries/lighthouse_utils",
    ],
    Projetos: [
      "projects/grazziotin",
      "projects/sao_joao_app",
    ],
    "CI/CD": ["ci_cd/como-gerar-versoes", "ci_cd/como-publicar-nas-lojas", "ci_cd/bitrise"],
  },
};
