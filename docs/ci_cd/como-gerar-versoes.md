---
id: como-gerar-versoes
title: Como gerar versões de apps na Lighthouse
---

# 💻

# Introdução

Existem duas maneiras de gerar versões de apps:

- Automaticamente (via [Bitrise](https://www.bitrise.io/))
- Manualmente (via gladlew no Android, via XCode no iOS)

Caso o Bitrise ainda não esteja configurado no seu projeto, veja a sessão sobre [Como configurar o Bitrise](./bitrise).

## Preparando uma versão do app

1. Certifique-se de que todas as alterações necessárias na versão já estejam mergeados com a branch `develop`
2. Crie uma nova branch, a partir da `develop`, seguindo o seguinte padrão de nomenclatura `release/*.*.*` (E.x: `release/2.5.1`)
3. No arquivo `package.json`, altere a propriedade `version` para o mesmo número de versão indicado no nome da branch (E.x: `"version": "2.5.1",`)
4. Garanta que o arquivo `.env`, `.env.production`, etc, esteja apontando **para o servidor correto**.

Caso você esteja fazendo um build de `Release`, geralmente o ambiente utilizado deve ser o de `produção` e quando for fazer um build do tipo `Staging`, irá apontar para `homologação`. Se você *não sabe*, **pergunte à equipe**.

Caso você esteja preparando uma versão que será gerada via Bitrise, **pode avançar para o próximo passo**.

Caso esteja gerando a versão manualmente, siga os seguintes passos:

**Android**

1. Acesse o arquivo `android\app\build.gradle`
2. Altere o número de versão do projeto na propriedade `versionName` e o número de compilação na propriedade `versionCode` maior que o último enviado à PlayStore

```
defaultConfig {
    versionCode 11503
    versionName "2.5.1"    
}
```

**iOS**

1. Acesso o arquivo `ios\NomeDoSeuApp.xcodeproj\project.pbxproj`
2. Altere o número da versão do projeto em **todas** as propriedades `MARKETING_VERSION`

```
MARKETING_VERSION = 2.5.1;
```
3. Altere o número de build em **todas** as propriedades `CURRENT_PROJECT_VERSION`, para um número maior do que ultima compilação enviada ao TestFlight

```
CURRENT_PROJECT_VERSION = 71;
```

### Como faço para saber o número de compilação da última versão enviada para o TestFlight 😱 !?

1. [Acesse o painel do app](https://appstoreconnect.apple.com/), usando uma conta com privilégios o suficiente para ver e enviar compilações ao TestFlight (solicitar à equipe)
2. Encontre a última versão enviada para a biblioteca de compilações e verifique o número de compilação

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/9f3c4c6d9e78cfe2125ef092276f60db/image.png" />

No caso do print, o número de compilação/build da última versão enviada era **70**

### Como faço para saber o número de compilação da última versão enviada para a Play Store 😨 !??

1. [Acesse o console de desenvolvedor](https://play.google.com/apps/publish/), usando uma conta com privilégios o suficiente para ver e enviar versões (solicitar à equipe)

2. Acesse a aba "Visão geral das versões"

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/5e323649937b604271ffc6acb83ebd05/image.png" />

3. Encontre a última versão enviada para a biblioteca de compilações e verifique a coluna "*Versão mais recente*". Esse é o número de compilação (**11502**)

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/79ba1ba732610009c7dac5e74c9117d4/image.png" />





## Automaticamente (via Bitrise)

### Code push (triggers)

Se o seu projeto está com os [triggers](https://devcenter.bitrise.io/builds/triggering-builds/trigger-code-push/) devidamente configurados no Bitrise, basta você dar um `git push` na sua branch da versão, ou publicá-la com `git push -u`, para iniciar o build de Release para ambas as plataformas.

### Iniciando o build no Bitrise manualmente

1. Acesse o painel do app no Bitrise, usando uma conta com privilégios o suficiente para gerar novas publicações (solicitar à equipe)
2. acesse a aba builds do projeto
3. Clique no botão roxo "Start/Schedule a build", no canto direito superior

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/ab56a11fb3ed23911914677097a636e9/image.png" />

4. Digite o nome da sua branch da nova versão no campo "Branch", e selecione qual é o fluxo de build que irá utilizar no campo "Workflow". Geralmente o nome será semelhante à `Production-AAB`, `iOS`, `Production-APK`, como mostrado no print

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/576e29da256332258853f82a412095c1/image.png" />

5. Clique em "Start Build"
6. Pronto!!

### Onde posso encontrar os artefatos gerados?

Os artefatos podem ter sido enviados para um dos seguintes lugares, dependendo de como o Bitrise estiver configurado em seu projeto:

- [AppCenter](https://appcenter.ms/apps) (solicite uma conta à equipe, se ainda não tiver)
- Bitrise
- Loja (PlayStore, faixa de teste interno; TestFlight)

#### Bitrise aba de Artifacts

1. Acesse a tela do seu build
2. Role a página até encontrar as abas "LOGS" e "APPS & ARTIFACTS"
3. Clique em "APPS & ARTIFACTS"
4. Seu build deverá estar disponível para download

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/dccad304cef17d93e8421fe3285d0ede/image.png" />

## Manualmente


### Android APK

1. 
   
```
cd android
```
2. 
   
```
gradlew assembleRelease
```

3. Seu APK estará na pasta `android/app/build/outputs/apk/release/app-release.apk`

### Android App Bundle (AAB)

1. 
   
```
cd android
```
2. 
   
```
gradlew bundleRelease
```

3. Seu AAB estará na pasta `android/app/build/outputs/bundle/release/app.aab`

### iOS (IPA)

1. Abra a pasta `ios/NomeDoSeuApp.xcodeworkspace` no XCode
2. Na aba "*Signing and capabilities*", marque a checkbox "*Automatically manage signing*" e garanta que o *Team* selecionado corresponde ao time da empresa do cliente (E.x: No projeto São João, o time é "Comercio de Medicamentos Brair")

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/dda1153d69378b1a1d44bb564934d5eb/image.png" />

3. Tenha certeza de que o dispositivo selecionado para debugging está como "Any iOS device". Isso garante que a opção "Archive" estará disponível

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/5b6e35cf3bd33c2a368d7b4c226a73ef/image.png" />

4. No menu superior, acesse: "Product/Scheme/Edit Scheme"

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/dc1f132191f2eff04d196b0dd1b6241d/image.png" />

5. Na aba "Run", garanta que a caixa de seleção "Build configuration" está marcada como "Release"

<img src="https://gitlab.com/lighthouseit/packages/documentation-v2/uploads/c7454c8b4f020b12da0d62820d1d0da1/image.png" />

6. Desmarque a checkbox "Debug executable"
7. Clique em fechar
8. No menu superior acesse "Product/Archive"
9.  Após o processo de archiving (empacotamento de release) for concluído, ele abrirá automaticamente a janela "Organizer". Para acessar essa janela manualmente, abra o menu superior "Window/Organizer"
10.   Seu build "IPA" estará disponível para distribuição nessa janela

## Publicando a compilação gerada

Para saber mais sobre como publicar uma versão na Play Store ou no TestFlight, veja a sessão [Como publicar uma versão nas lojas](./como-publicar-nas-lojas)